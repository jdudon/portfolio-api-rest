<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;


class FileService {

    public function upload(File $file, string $absolutePath = ""):string {
        
        $path = __DIR__."/../../public/upload";

        if(!is_dir($path)) {
            mkdir($path);
        }
        $filename = uniqid() . "." . $file->guessExtension();
        var_dump($path);
        var_dump($filename);
        var_dump($absolutePath . "/upload/" . $filename);
        $file->move($path, $filename);
        return "/upload/" . $filename;
    }
}
