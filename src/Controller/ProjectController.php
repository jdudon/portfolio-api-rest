<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Project;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileService;


/**
 * @Route("/api/project", name="api_project")
 */


class ProjectController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    
        /**
         * @Route("/", methods="GET")
         */
        public function all(){
            $repo = $this->getDoctrine()->getRepository(Project::class);
            $projects = $repo->findAll();
            $json = $this->serializer->serialize($projects, "json");
    
            return JsonResponse::fromJsonString($json);
        }
        
        /**
         * @Route("/", methods="POST")
         */
        public function add(Request $req, FileService $fileService) {
            //On récupère le fichier dans la request
            $image = $req->files->get("image");
            //On récupère l'url absolue de notre application
            $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
            //On utilise le fileService pour uploader l'image
            $imageUrl = $fileService->upload($image, $absoluteUrl);
    
            $project = new Project();
            $project->setDescription($req->get("description"));
            $project->setLink($req->get("link"));
            $project->setLinkTwo($req->get("linkTwo"));
            $project->setTitle($req->get("title"));
            $project->setImage($imageUrl);



            
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($project);
            $manager->flush();
    
            $json = $this->serializer->serialize($project, "json");
    
            return JsonResponse::fromJsonString($json);
    
    
        }

    /**
     * @Route("/project/{project}", methods={"DELETE"})
     */
    public function remove(Project $project)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($project);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/project/{project}", methods={"PUT"})
     */
    public function update(Project $project, Request $request)
    {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Project::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $student->setName($updated->getName());
        $student->setSurname($updated->getSurname());
        $student->setLevel($updated->getLevel());
        $student->setTech($updated->getTech());

        $manager->flush();
        return new Response("", 204);
    }
}
